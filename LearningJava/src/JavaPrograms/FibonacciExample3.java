/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package JavaPrograms;

/**
 *
 * @author Alex-THINK
 */
public class FibonacciExample3 {

    public static void main(String[] args) {

        int prev = 0, next = 1, result, n = 10;
        System.out.print(prev + " " + next);
        for (int i = 0; i < (n - 2); i++) {
            result = prev + next;
            System.out.print(" " + result);
            prev = next;
            next = result;
        }
    }
}
