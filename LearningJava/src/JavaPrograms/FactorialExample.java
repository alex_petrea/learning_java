/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package JavaPrograms;

/**
 *
 * this is the first example using loop
 *
 * @author Alex-THINK
 */
public class FactorialExample {

    public static void main(String[] args) {
        int i, fact = 1;
        int number = 5;//it is the number to calculate factorial
        for (i = 1; i <= number;i++) {
            fact = fact * i;
        }
        System.out.println("Factorial of" + number + "is: " + fact);

    }
}
