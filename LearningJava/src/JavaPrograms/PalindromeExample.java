/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package JavaPrograms;

/**
 *
 * @author Alex-THINK Get the number to check for palindrome Hold the number in
 * temporary variable Reverse the number Compare the temporary number with
 * reversed number if both numbers are same, print "palindrome number" Else
 * print "not palindrome number"
 */
public class PalindromeExample {

    public static void main(String[] args) {
        int r, sum = 0, temp;
        int n = 454; //it is te number variable to be checked for palindrome
        temp = n;
        while (n > 0) {
            r = n % 10;//getting remainder
            n = n / 10;
            sum = (sum * 10) + r;
        }

        if (temp == sum) {
            System.out.println("palindrome number");
        } else {
            System.out.println("not palindrome");
        }

    }

}

