/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package JavaPrograms;

/**
 *
 * @author Alex-THINK
 */
public class PrimeExample2 {

    static void checkPrime(int n) {
        if (n == 0 || n == 1) {
            System.out.println(n + " is not prime number");
            return;
        }
        
        int i;
        int flag = 0;
        int m = n / 2;
        
        for (i = 2; i < m; i++) {
            if (n % i == 0) {
                System.out.println(n + " is not prime number");
                flag = 1;
                break;
            }
        }
        if (flag == 0) {
            System.out.println(n + " is a prime number");
        }

    }

    public static void main(String[] args) {
        checkPrime(1);
        checkPrime(3);
        checkPrime(7);
        checkPrime(13);

    }
}
