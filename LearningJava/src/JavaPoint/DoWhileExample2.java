/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package JavaPoint;

/**
 *
 * @author Alex-THINK If you pass true in the do-while loop, it will be
 * infinitive do-while loop
 */
public class DoWhileExample2 {

    public static void main(String[] args) {
        do {
            System.out.println("infinitive do while loop");
        } while (true);
    }

}
