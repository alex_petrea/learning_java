/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package JavaPoint;

/**
 *
 * @author Alex-THINK If the number of iteration is not fixed and you must have
 * to execute the loop at least once, it is recommended to use do-while loop
 */
public class DoWhileExample {

    public static void main(String[] args) {
        int i = 1;
        do {
            System.out.println(i);
            i++;
        } while (i <= 10);
    }
}
